@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        @foreach($sherly as $data)
            <div class="card">
                <div class="card-header">
                <img src="{{ $data->avatar}}" class ="rounded-circle" alt="Mirip Sherly ga?" width ="5%" height="40%">
                {{$data->name}}
                </div>


                <div class="card-body">
                <img src="{{asset($data->image)}}" class ="card-img-top" alt="Mirip Sherly ga?" width ="304" height="500">
                {{$data->email}}
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
