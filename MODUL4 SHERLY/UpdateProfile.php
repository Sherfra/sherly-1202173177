
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <title>PROFILE EAD</title>
</head>

<body>
    <nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="Home.php">
            <img src="logoEAD.png" height="30px" class="d-inline-block align-top" alt="">
        </a>
        <div style="float: right;">
            <table>
                <tr>
                    <td><a href="Cart.php"><img src="cart.png" alt="Cart" width="20px"></a></td>
                    <td>
                        <div class="bs-example" style="padding: 5px;">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?= $_SESSION["login"] ?></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="UpdateProfile.php" class="dropdown-item">Profile</a>
                                <div class="dropdown-divider"></div>
                                <a href="logout.php" class="dropdown-item">Logout</a>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </nav>


<h3 style="text-align: center"> Update Profile</h3> <br>

<form action ="conf.php" method="POST" >
                     <tr>
                     <div class="form-group">
                     <td>
                     <label for="email">Email</label>
                     <input type="email" class="form-control" id="email" name ="email" placeholder="Email" required/>
                     <label readonly class="form-control-plaintext"><?=$_SESSION['email']?></label>
                </td>
                     </div>
                     </tr>
                     <tr>
                     <div class="form-group">
                     <td>
                     <label for="username">Username</label>
                     <input type="text" class="form-control" id="username" name="username" placeholder="username" required/>
                     <input type="text" name="update" class="form-control" value="<?=$_SESSION['username']?>">           
                </td>
                     </div>
                     </tr>
                     <tr>
                     <div class="form-group">
                     <td>
                     <label for="mobilenumber">Mobile Number</label>
                     <input type="text" class="form-control" id="mobilenumber" name="mobilenumber" placeholder="mobilenumber" required/>
                     <input type="number" name="numberUP" class="form-control" value="<?=$_SESSION['mobilenumber']?>">
                </td>
                     </div>
                     </tr>
                     <tr>
                     <div class="form-group">
                     <td>
                     <label for="newPassword"> New Password</label>
                     <input type="Password" class="form-control" id="newPassword" name="newPassword" placeholder="newPassword" required/>
                     </td>
                     </div>
                     </tr>
                     <tr>
                     <div class="form-group">
                     <td>
                     <label for="confpass">Confirm Password</label>
                     <input type="password" class="form-control" id="confpass" name="confpass" placeholder="confpass" required/>
                     </td>
                     </div>
                     </tr><br>
                             <tr>
                             <td>
                             <center>
                             <button type="submit" class="submit" name="submit" id="submit1" >Save</button>
                             <button type="reset" class="reset" name="reset" id="submit1"  >Cancel</button>
                             </center>  
                             </td>
</tr>                    
</center>
</table><br><br><br>
</form>
  <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

</body>
</html>
<?php
session_start();
if (!(isset($_SESSION["login"]))) {
    echo "<script>window.location.href='Home.php'</script>";
    exit;
}
?>