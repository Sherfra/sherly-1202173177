<?php
session_start();
if(!(isset($_SESSION["login"]))){
    echo "<script>window.location.href='Home.php'</script>";
        exit;
}
include "Conn.php";

?>
<!DOCTYPE html>
<html>

<head>
    <title>Cart</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet" type="text/css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
</head>

<body>

<nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="home.php">
            <img src="ead.jpg" height="40px" class="d-inline-block align-top">
        </a>
        <div style="float: right; margin-right: 40px;">
            <table>
                <tr>
                    <td><a href="cart.php"><img src="cart.png" alt="Cart" width="20px"></a></td>
                    <td>
                        <div class="bs-example" style="padding: 5px;">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?=$_SESSION["user"]?></a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a href="profil.php" class="dropdown-item">Profile</a>
                                <div class="dropdown-divider"></div>
                                <a href="logout.php" class="dropdown-item">Logout</a>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </nav>

<table class="table table-bordered table-dark">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Product</th>
      <th scope="col">Price</th>
      <th scope="col">crud</th>
    </tr>
  </thead>
</table>
<?php
    $id = $_SESSION["id"];
    $tampil = mysqli_query($connect, "SELECT * FROM cart WHERE user_id = '$id'");
    $count=1;
    $price=0;
        while($row = mysqli_fetch_array($tampil)){
            echo "<tr>";
            echo "<td>$count</td>";
            echo "<td>";
            echo $row['product'];
            echo "</td>";
            echo "<td>";
            echo $row['price'];
            echo "</td>";
            echo "<td><a href='hapus.php?id=".$row['id']." 'name='delete' class='btn btn-danger' data-dismiss='modal'>&times</a></td>";
            $count=$count+1;
            $price=$price+$row['price'];
            echo "</tr>";
        }
        
  ?>
</tbody>
    <tr>
        <td colspan="2"><b>Total</b></td>
        <td colspan="2"><b><?=$price?></b></td>
    </tr>
</table>
<footer class="footer mt-auto py-3" Style ="background-color : softpink">
       <div class="container">
           <b><span class="text-muted">© EAD STORE</span></b>
           </div>
           </footer>
           <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
           <script>window.jQuery || document.write('<script src="/docs/4.3/assets/js/vendor/jquery-slim.min.js"><\/script>')</script><script src="/docs/4.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-xrRywqdh3PHs8keKZN+8zzc5TX0GRTLCcmivcbNJWm2rs5C8PRhcEn3czEjhAO9o" crossorigin="anonymous"></script></body>
       </html>
       
</body>
</html>