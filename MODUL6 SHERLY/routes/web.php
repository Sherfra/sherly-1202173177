<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/AddNewPost','AddNewPostController@index')->name('AddNewPost');
Route::get('/Profile','EditProfileController@index')->name('Profile');
Route::get('/EditProfile','EditProfileController@edit')->name('EditProfile');
Route::get('likeposts{id}','HomeController@like')->name('Like');
Route::get('DetailsPost/{id}','HomeController@specific')->name('DetailsPost');
Route::post('/home','HomeController@store')->name('home');
Route::resource('AddNewPost','AddNewPostController');


