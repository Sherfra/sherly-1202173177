<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class posts extends Model
{
    protected $table='post';
     protected $fillable = [
        'id','user_id','caption', 'image','create_at','update_at',
    ];   

    public function comment(){
        return $this->hasMany('App\comment','post_id','id');
    }
    public function users(){
        return $this->belongsto('App\User','user_id');
    }
}
