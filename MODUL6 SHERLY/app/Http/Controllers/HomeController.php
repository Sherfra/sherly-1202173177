<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\posts;
use App\User;
use App\comment;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts= posts::get();        
        return view('home',[ 'post'=>$posts]);
       
    }
    public function like($id){
        posts::find($id)->increment('likes');
        return redirect('home');
    }
    public function specific($id){
        // $user= DB::table ('post')
        // ->join ('users','post.user_id','=','user_id')
        // ->select('users.*')
        // ->first();

        // $post= DB::table('komentar_posts')
        // ->join ('post','komentar_posts.post_id','=','post.id')
        // ->select('komentar_posts.*','post.*')->where('komentar_posts.post_id','=',$id)
        // ->first();

        // $komen= DB::table('komentar_posts')
        // ->join ('users','komentar_posts.user_id','=','users.id')
        // ->select('komentar_posts.*','users.*')->where('komentar_posts.post_id','=',$id)
        // ->get();

        // return view('DetailsPost',[
        //     'id'=>$post,
        //     'user'=>$user,
        //     'komen'=>$komen
        // ]);
        $posts = posts::find($id);
        return view('DetailsPost',['posts'=>$posts]);
    }

    public function addkomen(posts $post)
    {
      //
    }
        public function store(){
            dd(request()->all());
        }
        
}
//     public function addkomen(Request $request){
//     $comment = new Comment();
//     $comment->user_id =$request->user_id;
//     $comment->post_id =$request->post_id;
//     $comment->comment =$request->comment;
//     $comment->save();
//     return redirect('Profile');
// }

        
    

