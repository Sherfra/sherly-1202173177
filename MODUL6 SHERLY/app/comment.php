<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class comment extends Model
{
    protected $table ="komentar_posts";
    protected $fillable = [
        'id','user_id','post_id', 'comment','create_at','update_at',
    ];
     public function post(){   
    return $this->belongsTo('App\posts','post_id','id');
     }
     public function user(){
    return $this ->belongsTo('App\User','user_id','id');
}
}

