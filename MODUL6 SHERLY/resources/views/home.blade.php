@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-5">
            @foreach($post as $pos)
            <div class="card">
                <div class="card-header">
                    <div class="container ">
                        <div class="row">
                            <div class="col-2">
                                <img src="{{$pos->users['avatar']}}" class="rounded-circle" alt="kosonggg" width="100%"
                                    height="35">
                            </div>
                           
                                <span class="text-dark"><b>{{$pos->users['name']}}</b></span> </a>
                        </div>
                    </div>

                </div>
                <a href="{{route('DetailsPost',$pos->id)}}">
                     <img src="{{ $pos->image }}" style="width:100%; height:350px;" alt="kosong"></a>
                <div class="card-header">
                    <div class="container">
                        <p>
                    
                            <a href="{{ route('Like',['id'=>$pos->id]) }}" class="">
                            <button class="btn" name="direct" value="home" type="submit"><i class="fa fa-heart-o"></i></button>

                                <a href="/Detailpost"><i class="fa fa-comment-o"></i></a>
                                <p><b>{{$pos->likes}}</b>likes</p>
                           
                                    <span class="text-dark"> <b>{{$pos->users['email']}}</b></a>
                                <p>{{ $pos->caption }}</p>
                                   

                                <form method="" enctype="multipart/form-data">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control" placeholder="Recipient's Name"
                                            name="komen">
                                        <div class="input-group-append">
                                            <button class="btn btn-success" type="submit"
                                                style="background-color:grey;">Post</button>
                                        </div>
                                    </div>

                    </div>
                </div>
            </div>
            <br>
            @endforeach
        </div>
    </div>
    @endsection
