@extends('layouts.app')

@section('content')

<div class="row">
             <div class="col-8 offset-2">
 
               <div class="row">
                    <h1 style="color:magenta">Add New Post</h1>
                   </div>
                   <label for="caption" class="col-md-4 col-form-label">Post Caption</label>
                   <form action="{{route('AddNewPost.store')}}" method="post" enctype="multipart/form-data">
                   {{csrf_field()}}
    
       <input type="hidden" value="{{Auth::id()}}" name="id">
       <input type="text" name="caption"> <br>
       <label for="caption" class="col-md-4 col-form-label">Post Image</label> <br>
       <input type="file" name="foto"> <br> <br>
       <input type="submit" value="Add New Post" style="color:blue"> <br>
</form>

@endsection