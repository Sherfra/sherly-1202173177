@extends('layouts.app')
@section('content')

<div class="card mb-3" style="max-width: 100%;">
  <div class="row no-gutters">
    <div class="col-md-4">
      <img src="../{{$posts->image}}" class="card-img" alt="post_image">
    </div>
    <div class="col-md-8">
      <div class="card-body">
        <h5 class="card-title">{{$posts->users['email']}}</h5>     
        <h5 class="card-title" >{{$posts->caption}}</h5>
        @foreach($posts->comment as $cm)
        {{$cm['comment']}}
        @endforeach
        <hr>
      </div>
    </div>
  </div>
</div>
@endsection
