<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([ 
            'name' => 'test', 
            'email' => 'test@gmail.com', 
            'password' => bcrypt('12345678'), 
            'avatar' =>'img/pinkpanther.jpg', 
             
        ]); 
        DB::table('users')->insert([ 
            'name' => 'sherfra', 
            'email' => 'sherfra11@gmail.com', 
            'password' => bcrypt('felysha11'), 
            'avatar' =>'img/wonderwoman.jpg', 
             
        ]); 

    }
}
